

@extends("template")
@section("titre")
Commande
@endsection

@section("content")
<a href="/commande/create" class="btn btn-primary mx-5 my-1">Ajouter</a>

<div class="row">
    @foreach ($lesCommandes as $commande)
    <div class="card m-2" style="width: 18rem;">
        <div class="card-body">
          <h5 class="card-title">{{$commmande->id}}</h5>
          <p class="card-text">Some quick example text to build on the card title and make up the bulk of the card's content.</p>
          <a href="commande/{{$commande->id}}" class="btn btn-primary">Consulter</a>

          <a href="commande/{{$commande->id}}/edit" class="btn btn-warning">Modifier</a>
          <form class="col-2 " action="/commande/{{$commande->id}}" method="post">
            @method("delete")
            @csrf
            <button type="submit" class="btn btn-danger">Supprimer</button>
            </form>

        </div>
      </div>

    @endforeach
</div>
@endsection
