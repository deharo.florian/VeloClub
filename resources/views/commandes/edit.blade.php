@extends("template")
@section("titre")
Commande formulaire
@endsection

@section("content")
{{-- @dd($_POST); --}}
<div class="container">


    <h1>Formulaire d'ajout</h1>
    <form method='post' action="/commande" >
            @csrf
            @error('nom')
               <div class='alert alert-danger mt-1'>{{$message}}</div>
            @enderror

       <div class="form-floating mb-3 ">
          <input type="date" class="form-control" id="date_commande"  placeholder="" name="date_commande" value="{{old("date_commande") ?? $commande->date_commande   }}">
          <label for="date_commande">date commande</label>
       </div>
       @error('image_categorie')
          <div class='alert alert-danger mt-1'>{{$message}}</div>
       @enderror
       <div class="form-floating mb-3 ">
        <input type="date" class="form-control" id="date_livraison" placeholder="" name="date_livraison" value="{{old("date_livraison") ?? $commande->date_livraison  }}">
        <label for="date_livraison">date livraison</label>
     </div>
     <div class="form-floating mb-3 ">
        <input type="text" class="form-control" id="etat" placeholder="" name="etat" value="{{old("etat") ?? $commande->etat  }}">
        <label for="etat">etat</label>
     </div>
     <div class="form-floating mb-3 ">
        <input type="textutilisateurs_id " class="form-control" id="utilisateurs_id " placeholder="" name="utilisateurs_id " value="{{old("utilisateurs_id ")  }}">
        <label for="utilisateurs_id ">Client</label>
     </div>

       <button class="btn btn-primary">
          Ajouter
       </button>
    </form>
</div>

@endsection
