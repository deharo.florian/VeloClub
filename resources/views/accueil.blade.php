
@extends("template")
@section("titre")
VeloClub : Accueil
@endsection
<style>

</style>

@section("content")

    <!-- contenu > accueil -->
    <div class="container-fluid container_accueil">
        <div class="row">
            <div class="col offset-1 text-center text-sm-start">
                <h1 class='display-1 text-danger '>Velo Club</h1>
                <h2>Surpassez vos limites ! </h2>
            </div>
        </div>
    </div>

    <!-- description du site -->
    <div class="container-fluid grise py-5">
        <div class="row">
            <div class="col col-6 offset-3 col-md-4 offset-md-4 text-light text-center">
                <h2>HISTOIRE DU VELOCLUB</h2>
                <p >Lorem ipsum dolor sit amet consectetur adipisicing elit. Velit eaque quaerat illum
                    voluptas inventore laboriosam ratione. Molestiae, quia distinctio consequuntur ullam facilis
                    laboriosam quos, repellat, maiores ad consequatur fugit animi?</p>
            </div>
        </div>
    </div>
        <!-- carroussel categorie velo -->

    <div class="container-fluid rose">
        <div class="row py-5">
            <div class="col col-8 offset-2">
                <h3 class='display-3 text-center pb-5'><b>Les categories de vélo</b></h3>
                <div id="carouselExampleCaptions" class="carousel slide pb-5" data-bs-ride="carousel">
                    <div class="carousel-indicators">
                <!-- premier foreach  -->
                         <!-- premier foreach  -->

                         <?php  $i = 0 ;?>
                        @foreach($lesCategories as $categorie)
                            @if($i==0)
                            <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="{{ $categorie->id - 1 }}" aria-label="Slide {{ $categorie->id }} " class="active" aria-current="true" ></button>

                            @else

                                <button type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide-to="{{ $categorie->id - 1 }}" aria-label="Slide {{ $categorie->id }}"  ></button>
                            @endif
                        <?php   $i++ ?>
                        @endforeach
                   </div>
                    <div class="carousel-inner">
                       <!-- deuxieme foreach -->
                       <?php  $a = 0 ; ?>
                            @foreach($lesCategories as $categorie)
                                @if($a==0)
                                    <div class="carousel-item active">
                                        <a href=""><img src="/storage/{{$categorie->image_categorie}}"   alt="premiere"></a>
                                        <div class="carousel-caption d-none d-sm-block">
                                            <h3>{{$categorie->nom}}</h3>
                                        </div>
                                    </div>
                                @else

                                    <div class="carousel-item">
                                        <a href=""><img src="/storage/{{ $categorie->image_categorie}}"  class="d-block " alt="seconde"></a>
                                        <div class="carousel-caption d-none d-sm-block">
                                            <h3>{{$categorie->nom}}</h3>
                                        </div>
                                    </div>
                                @endif
                            <?php   $a++ ?>
                            @endforeach
                    </div>
                    <button class="carousel-control-prev" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="prev">
                        <span class="carousel-control-prev-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Previous</span>
                    </button>
                    <button class="carousel-control-next" type="button" data-bs-target="#carouselExampleCaptions" data-bs-slide="next">
                        <span class="carousel-control-next-icon" aria-hidden="true"></span>
                        <span class="visually-hidden">Next</span>
                    </button>
                 </div>
            </div>
        </div>
    </div>



@endsection
