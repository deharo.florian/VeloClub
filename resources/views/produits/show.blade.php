
@extends("template")
@section("titre")
Produit
@endsection

@section("content")
{{-- <div style='padding-top:5%'></div> --}}

<div class="container">
    <div class="row" style='height:auto'>
        <div class="col col-12 col-md-7">
            <img class='img-fluid' src="/storage/{{$produit->image}}" alt="" >
        </div>
        <div class="col my-5 col-12 col-md-5">
    <h1>{{$produit->modele }} </h1>
    <p class='h2 bg-danger'><b>{{$produit->prixUnitaire }} €</b></p>

    {{-- @dd($produit->commentaires) --}}
    @if ( !empty($produit->commentaires) )
    @php
        $moyenne = 0
    @endphp
        @foreach ($produit->commentaires as $commentaire)
           @php
            $moyenne = $commentaire->avg('note')
           @endphp
        @endforeach
       @for ($i = 0;$i < $moyenne ;$i++)
            <img style="width:6%" src="https://img.icons8.com/fluency/48/undefined/star.png"/>
        @endfor


    @endif
    <p>{{ $produit->description}} </p>
    @guest
    <div class="alert alert-warning">
        <b>
            Pour ajouter un article a votre panier il faut <a class="link-dark" href="/register"> vous inscrire </a>
            ou <a class="link-dark" href="/login">vous connecter</a>
        </b>
    </div>

    @endguest

    @auth
    <form class="form" action="/client/panier" method="post">
        @csrf
        <h2>Ajouter au panier</h2>
        <input type="hidden" name="idProduit" value="{{$produit->id}}">
        <label for="nbPlaces">Nombre d'articles :</label>
        <input class="form-control my-2" id="nbPlaces" placeholder="nombre de produit" type="number" min="0" max="20"
            required name="nb">
        <button class="btn btn-warning">Ajouter</button>
    </form>
    @endauth

    </div>
    </div>
</div>

{{-- section commentaire --}}
<hr class="my-2">
<div class="row mx-auto row-cols-1">
    <h2>Commentaires </h2>
    <div class="container">
        @auth
        <div class="col-lg-10 mx-auto mb-3">
            <form action="/client/commentaires" method="post">
                @csrf
                <input type="hidden" name="produit_id" value="{{$produit->id}}">
                <div class="form-floating my-2">
                    <textarea name="texte" required maxlength="255" minlength="4" class="form-control"
                        placeholder="Leave a comment here" id="floatingTextarea2"
                        style="height: 100px">{{old("commentaire")}}</textarea>
                    <label for="floatingTextarea2">Commentaire</label>
                </div>
                @error('texte')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
                <div class="form-floating my-1">
                    <p >Attribuez une note</p>
                    <input type="number" min="1" max="5" required name="note" id="note" >

                </div>
                @error('note')
                <div class='alert alert-danger mt-1'>{{message}}</div>
                @enderror
                <button type="submit" class="btn btn-primary">Envoyer</button>
            </form>
        </div>
        @endauth
    </div>
    @foreach ($produit->commentaires->sortDesc()->forPage($nPage??1, 6) as $unCommentaire )
    <div class="alert alert-dark px-2 py-1 col-lg-10 mx-auto mt-1">
        <h5 class="alert-heading">{{Str::ucfirst($unCommentaire->autheur->prenom)}}</h4>
            <hr class="mt-1 mb-2">
            <p>
            @for ($i = 0;$i < $unCommentaire->note;$i++)
             <img style="width:2%" src="https://img.icons8.com/fluency/48/undefined/star.png"/>
            @endfor </p>

            <p>{{$unCommentaire->texte}}</p>
            <p class="text-muted">{{$unCommentaire->created_at}} {{$unCommentaire->created_at !=
                $unCommentaire->updated_at ? "(modifier)" : "" }}</p>
    </div>
    @endforeach
</div>

@endsection
