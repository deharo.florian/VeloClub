@extends("template")
@section('titre')
    Produit
@endsection

@section('content')

<div class="container-fluid container_accueil_velo">
    <div class="row">
        <div class="col offset-1 text-center text-sm-start">
            <h1 class='display-1 text-danger '>Velo Club</h1>
            <h2 class="h4 text-secondary"> les produits</h2>

        </div>
    </div>
</div>
    @if (session()->get('success'))
        <div id="notif" class="alert alert-success">
            {{ session()->get('success') }}
        </div>
    @endif

    <div class="container">
        <div class="row text-center py-5">
            <div class="col bg-danger"><h2>Nos produits</h2></div>

        </div>

        <div class="row">

            <form action="" method="get">
                <h2>Recherche de produits</h2>
                <div class='row mb-2'>
                    <label for='modele'>Recherche par modele </label>
                    <input value='{{old("modele")}}' name='modele'  type='text' class="form-control" id="modele"
                        placeholder="modele">
                    @error('modele')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                    @enderror
                </div>
                <div class='row mb-2'>
                    <label for="">enfant ou adulte</label>
                    <select name="enfant" id="" class="form-control">
                        <option selected disabled value="">Filtre par taille</option>
                        <option value="1">enfant</option>
                        <option value="0">adulte</option>

                    </select>
                    @error('enfant')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                    @enderror
                </div>
                <div class='row mb-2'>
                    <label for="">Type</label>
                    <select name="types" id="" class="form-control">
                        <option selected disabled value="">Filtre par type</option>
                        @foreach ($lesTypes as $unType )
                        <option value="{{$unType->id}}">{{Str::ucfirst($unType->nom)}}</option>
                        @endforeach
                    </select>
                    @error('types')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                    @enderror
                </div>
                <div class='row mb-2'>
                    <label for="">Categories</label>
                    <select name="categories" id="" class="form-control">
                        <option selected disabled value="">Filtre par categories</option>
                        @foreach ($lesCategories as $uneCategorie )
                        <option value="{{$uneCategorie->id}}">{{Str::ucfirst($uneCategorie->nom)}}</option>
                        @endforeach
                    </select>
                    @error('categorie')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                    @enderror
                </div>
                <button class="btn btn-secondary"> <i class="bi bi-search"></i> Recherche</button>
            </form>
        </div>

        <div class="row d-flex justify-content-center justify-content-sm-start">
    @if (auth()->user() && auth()->user()->role->nom == 'admin')
            <div class="row">
                <div class="col">
                    <a href="/admin/produit/create"  class="btn btn-info mx-5 my-1">Ajouter</a>

                </div>
            </div>
    @endif
    <div class="d-flex justify-content-center my-3">{{ $lesProduits->links() }}</div>


        @foreach ($lesProduits as $produit)
            <div class="card m-2" style="width: 18rem;">
                <img src="/storage/{{ $produit->image }}" class="card-img-top" alt="...">
                <div class="card-body">
                    <h5 class="card-text"> {{ $produit->modele }}</h5>
                    <p class="card-text alert-danger"><b> {{ $produit->prixUnitaire }}€</b></p>
                    @if (auth()->user() && auth()->user()->role->nom == 'admin')
                    <p class="text-center h5">id = {{ $produit->id }}</p>

                    <p class="card-text">
                        @if ($produit->estDisponible == true)
                            disponible
                        @else
                            indisponible
                        @endif
                    </p>

                    <p class="card-text">pour @if ($produit->enfant == true)
                            enfant
                        @else
                            adulte
                        @endif
                    </p>
                    @endif


                    <a href="/produit/{{ $produit->id }}" class="text-danger">Description +</a>
                    @if (auth()->user() && auth()->user()->role->nom == 'admin')
                    <div class="row mt-3">
                        <div class="col">
                            <a href="/admin/produit/{{ $produit->id }}/edit/" class="btn btn-warning">Modifier</a>

                        </div>
                        <div class="col">

                            
                            <form action="/admin/produit/{{ $produit->id }}" method="post">
                                @csrf
                                @method("delete")
                                <input class="btn btn-danger" type="submit" value="Supprimer">
                            </form>
                        </div>
                    </div>
                    @else

                            <form class="form" action="/client/panier" method="post">
                                @csrf
                                <input type="hidden" name="idProduit" value="{{$produit->id}}">
                                <label for="nbPlaces">Nombre d'articles :</label>
                                <input class="form-control my-2" id="nbProd" placeholder="nombre de produits" type="number"
                                    min="0" max="20" required name="nb">
                                <button class="btn btn-danger">Ajouter</button>
                            </form>

                    @endif
                </div>
            </div>
        @endforeach
    </div>
    </div>
@endsection
