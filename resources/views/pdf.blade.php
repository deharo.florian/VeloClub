<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Document</title>
</head>
<style>


    .tableauProduit th{
          background : black ;
          color :white ;
      }
      .tableauProduit td{
        border-bottom : 1px solid black ;
        border-left : 1px solid black ;

        /* background: grey; */
      }
    .tableauProduit{
      border :1px solid black ;
          width:100%;
      margin-top:5%;
          text-align: center;
    }


    .tableauPrix{
      /* display:flex; */
      border :solid 1px grey ;
      background: #ebebeb;
      margin-top:5%;
      margin-left:70%;
    }

    .clientPDF{
      margin-left:70%;
    }
  </style>
<body>
    {{-- @dd($commande) --}}
    <h1>VELOCLUB</h1>
<p>94 avenue de la fierté <br> PARIS <br> 75016</p>
<div class="clientPDF">
<h2>{{ $utilisateur->nom_client}} {{$utilisateur->prenom}} </h2>
<p>{{$utilisateur->adresse}}<br>
 {{$utilisateur->ville}}<br>Commande N° {{$commande->id}}<br>date:  {{$commande->dateFR($commande->date_commande)}} <br></p>
</div>
<!-- tableau des produits -->
<table class="tableauProduit">
        <tr>
                 <th >Produit</th>
                 <th >Prix Unit</th>
                 <th >Quantite</th>
                 <th >Montant</th>
        </tr>



            @foreach($produits as $produit)

              <tr>
                 <td > {{$produit->modele}}</td>
                 <td> {{$produit->prixUnitaire}} €</td>
                 <td>{{$produit->quantite}}</td>
                 <td>{{ $produit->quantite * $produit->prixUnitaire }} €</td>
               </tr>
               @endforeach

</table>
<!-- tableau total prix -->
<table class="tableauPrix">
 <tr >
            <td>Total HT</td>
            <td>{{$commande->calcTotal()}} €</td>

          </tr>
          <tr >
            <td>TVA(19.6%) </td>
            <td> {{round($commande->calcTotal()*0.196,2)}} €</td>

          </tr>
          <tr >
            <td>Frais de port </td>
            <td>50 €</td>

          </tr>
          <tr >
            <td>TOTAL TTC </td>
            <td> {{round($commande->calcTotal()*1.196,2) + 50}} €</td>

          </tr>
 </table>

</body>
</html>
