@extends("template")
@section("titre")
Mes commandes
@endsection
@section("content")
    <div class="container">
        <h1 class="my-5">Mes commandes</h1>


        <table class="table my-5 table-stripped">
            <thead>
                <tr>
                    <th>N°</th>
                    <th>Date</th>
                    <th>Statut</th>
                    {{-- <th>modele</th> --}}
                    <th>Prix Total</th>
                    <th>factures</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($commandes as $uneCommande )
                <tr>
                    {{-- @dd($uneCommande) --}}
                    <td>{{$loop->iteration}}</td>
                    <td>{{$uneCommande->created_at}}</td>
                    <td>{{$uneCommande->etat}}</td>
                    {{-- <td>
                        <ul>
                            @foreach ($uneCommande->produitCommande as $uneLigne  )
                            <li class="mb-2"><b>{{$uneLigne->nom}}</b> <br>
                            Places : {{$uneLigne->pivot->nbPlaces}}</li>
                            @endforeach
                        </ul>
                    </td> --}}
                    <td> {{round($uneCommande->calcTotal()*1.196,2) + 50}} €</td>
                    <td>
                        <form action="{{ URL::to('/client/pdf') }}" method="GET">
                        <input type="hidden" name="id" value="{{$uneCommande->id}}">
                        <input type="submit" value="voir"></form>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
