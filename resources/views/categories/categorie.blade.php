

@extends("template")
@section("titre")
Categorie
@endsection

@section("content")
<h1 class="text-center py-4">Les Categories</h1>
@if (auth()->user() && auth()->user()->role->nom=="admin")
<a href="/admin/categorie/create" class="btn btn-primary mx-5 my-1">Ajouter</a>
@endif
<div class="container-fluid">

<div class="row">
    @foreach ($lesCateg as $categorie)
    <div class="col-12 col-md-6">

        <div class="card  my-1" >
            <img src="/storage/{{$categorie->image_categorie}}" class="card-img-top" alt="{{$categorie->image_categorie}}">
            <div class="card-body">
                <h5 class="card-title">{{$categorie->nom}}</h5>
                <div class="row">
                    <div class="col">
                    </div>

                @if (auth()->user() && auth()->user()->role->nom=="admin")
                <div class="col">
                    <a href="/admin/categorie/{{$categorie->id}}/edit" class="btn btn-warning">Modifier</a>
                </div>
                <div class="col">
                    <form class="" action="/admin/categorie/{{$categorie->id}}" method="post">
                        @method("delete")
                        @csrf
                        <button type="submit" class="btn btn-danger">Supprimer</button>
                    </form>

                </div>
                @endif
                </div>

            </div>
        </div>

    </div>
    @endforeach
</div>
</div>

@endsection
