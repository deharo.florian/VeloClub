@extends("template")
@section("titre")
Statistiques
@endsection

@section("content")
<div class="container-fluid">
	<h1 class="text-center my-5">Statistiques</h1>

	<!-- diagramme par categorie -->
    <div class="divform px-5 pb-4">

        <div class="row my-3">
            <p class="h3 my-3 text-center bg-dark text-light  py-1">Categories</p>
            <div class="col col-12 col-md-4 d-flex align-items-center">
                <div class="" style="width:100%">
                    <table class="table table-striped table-hover table-bordered">
                        <thead>
                            <td>
                                #
                            </td>
                            <td>
                                Categorie
                            </td>
                            <td class="text-center">
                                nombre de produits vendus par categories
                            </td>
                            <td class="text-center">
                                chiffre d'affaire par categories
                            </td>
                        </thead>
                        @foreach ($categories as $categorie)
                        <tr>
                                <td>{{$loop->iteration}}</td>
                                <td>{{$categorie->nom}}</td>
                                <td  class="text-center">{{$categorie->laquantite}}</td>
                                <td class="text-center">{{$categorie->lasomme}} €</td>
                        </tr>
                        @endforeach
                    </table>
                </div>
            </div>
            <div class="col col-12 col-md-4" >
                <div  id="mon-chart"  ></div>
            </div>
            <div class="col col-12 col-md-4" >
                <div  id="mon-chart5"  ></div>
            </div>
        </div>


        <div class="row">
                <div class="col col-12 col-md-4 offset-4">
                    <form action="" method="get" class="d-flex ">

                            <select name="categ" id="" class="form-control mx-2">
                                <option selected disabled value="">Filtre par categories</option>
                                @foreach ($categories as $uneCategorie )
                                <option value="{{$uneCategorie->id}}">{{Str::ucfirst($uneCategorie->nom)}}</option>
                                @endforeach
                            </select>
                            @error('categ')
                            <div class='alert alert-danger mt-1'>{{message}}</div>
                            @enderror

                            <button class="btn btn-dark my-1"> <i class="bi bi-search"></i> Appliquer</button>
                        </form>
                    </div>
                </div>
        @if (isset($categOk) && $categOk == "ok")
        <div class="row">

            <div class="col d-flex align-items-center col-12 col-md-4">
                <div class="" style="width:100%">
                    <table class="table  table-striped table-hover table-bordered">
                        <thead>
                            <td>
                                #
                            </td>
                            <td>
                                Modele
                            </td>
                            <td class="text-center">
                                Nombre de produits vendus
                            </td>
                            <td class="text-center">
                                Chiffre d'affaire par produits vendus
                            </td>
                        </thead>
                        <tr>
                            @foreach ($produits as $produit)
                            {{-- @dd($categorie) --}}
                            <td>{{$loop->iteration}}</td>
                            <td>{{$produit->modele}}</td>
                            <td  class="text-center">{{$produit->laquantite}}</td>
                            <td class="text-center">{{$produit->lasomme}} €</td>
                        </tr>
                        @endforeach

                    </table>
                </div>
            </div>
            <div class="col col-12 col-md-4">
                <div id="mon-chart2"  ></div>

            </div>
            <div class="col col-12 col-md-4">
                <div id="mon-chart6"  ></div>

            </div>
        </div>

        @endif
    </div>
    {{-- diagramme types --}}
    <div class="divform px-5 pb-4">


        <div class="row">
            <p class="h3 my-3 text-center  bg-dark text-light py-1">Types</p>


            <div class="col d-flex align-items-center col-12 col-md-4">
                <div class="" style="width:100%">

            <table class="table table-striped table-hover table-bordered">
                <thead>
                    <td>
                        #
                    </td>
                    <td>
                      Nom
                    </td>
                    <td class="text-center">
                        nombre de produits vendus par types
                    </td>
                    <td class="text-center">
                        Chiffre d'affaire par types
                    </td>
                </thead>
                <tr>
            @foreach ($types as $type)
            {{-- @dd($categorie) --}}
                    <td>{{$loop->iteration}}</td>
                    <td>{{$type->nom}}</td>
                    <td  class="text-center">{{$type->laquantite}}</td>
                    <td  class="text-center">{{$type->lasomme}} €</td>

                </tr>
            @endforeach

              </table>
            </div>
        </div>
      <div class="col col-12 col-md-4" >
            <div  id="mon-chart4"  ></div>
        </div>
        <div class="col col-12 col-md-4" >
            <div  id="mon-chart8"  ></div>
        </div>
    </div>
    <div class="row">
        <div class="col  col-12 col-md-4 offset-md-4">


                <form action="" method="get" class="d-flex ">

                    <select name="types" id="" class="form-control">
                        <option selected disabled value="">Filtre par types</option>
                        @foreach ($toutType as $unTYpe )
                        <option value="{{$unTYpe->id}}">{{Str::ucfirst($unTYpe->nom)}}</option>
                        @endforeach
                    </select>
                    @error('categorie')
                    <div class='alert alert-danger mt-1'>{{message}}</div>
                    @enderror

                    <button class="btn btn-dark my-1"> <i class="bi bi-search"></i> Appliquer</button>
                </form>

        </div>
    </div>
    @if (isset($idtypesOk) && $idtypesOk == "ok")

    <div class="row">
        {{-- tableau types --}}

        <div class="col d-flex align-items-center col-12 col-md-4">
            <div class="" style="width:100%">
                <table class="table  table-striped table-hover table-bordered">
                    <thead>
                        <td>
                            #
                        </td>
                        <td>
                            Nom
                        </td>
                        <td class="text-center">
                            nombre de produits vendus
                        </td>
                        <td class="text-center">
                            nombre de produits vendus
                        </td>
                    </thead>
                    <tr>
                        @foreach ($produitstype as $produit)
                        {{-- @dd($categorie) --}}
                        <td>{{$loop->iteration}}</td>
                        <td>{{$produit->modele}}</td>
                        <td  class="text-center">{{$produit->laquantite}}</td>
                        <td  class="text-center">{{$produit->lasomme}} €</td>

                    </tr>
                    @endforeach

                </table>
            </div>
        </div>

                {{-- diagramme camembert types --}}
                <div class="col col-12 col-md-4">

                    <div id="mon-chart3"   ></div>
                </div>
                <div class="col col-12 col-md-4">

                    <div id="mon-chart7"  ></div>
                </div>

    </div>
    @endif
   </div>

</div>

@endsection
<!-- Importation de l'API AJAX Google Charts -->
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">

  google.charts.load('current', {'packages':['corechart']});
  google.charts.setOnLoadCallback(drawChart);
  google.charts.setOnLoadCallback(drawChart1);
  google.charts.setOnLoadCallback(drawChart2);
  google.charts.setOnLoadCallback(drawChart3);
  google.charts.setOnLoadCallback(drawChart4);
  google.charts.setOnLoadCallback(drawChart5);
  google.charts.setOnLoadCallback(drawChart6);
  google.charts.setOnLoadCallback(drawChart7);

//    proportion categorie

  function drawChart() {

    var data = google.visualization.arrayToDataTable([
      ['Catégories', 'quantite'],
      @foreach ($categories as $category) // On parcourt les catégories

      [ "{{ $category->nom }}", {{ $category->laquantite }} ], // Proportion des produits de la catégorie
      @endforeach
    ]);

    var options = {
      title: 'Proportion des produits vendus par catégorie', // Le titre
      is3D : true, // En 3D
      slices: {
            0: { color: 'orange' },
            1: { color: 'brown' },
            2: { color: 'RosyBrown' }
          }
    };

    // On crée le chart en indiquant l'élément où le placer "#mon-chart"
    var chart = new google.visualization.PieChart(document.getElementById('mon-chart'));

    // On désine le chart avec les données et les options
    chart.draw(data, options);
  }

//   proportion C.A. par categorie

  function drawChart4() {

var data = google.visualization.arrayToDataTable([
  ['Catégories', 'Produits'],
  @foreach ($categories as $category) // On parcourt les catégories

  [ "{{ $category->nom }}", {{ $category->lasomme }} ], // Proportion des produits de la catégorie
  @endforeach
]);

var options = {
  title: 'proportion C.A. par categorie', // Le titre
  is3D : true, // En 3D
  slices: {
        0: { color: 'blue' },
        1: { color: 'skyblue' },
        2: { color: 'MediumSlateBlue' }
      }
};

// On crée le chart en indiquant l'élément où le placer "#mon-chart"
var chart = new google.visualization.PieChart(document.getElementById('mon-chart5'));

// On désine le chart avec les données et les options
chart.draw(data, options);
}



//  proportion produit vendu par categorie

  function drawChart1() {

var data = google.visualization.arrayToDataTable([
  ['Catégories', 'Produits'],
  @foreach ($produits as $produit) // On parcourt les catégories

  [ "{{ $produit->modele }}", {{ $produit->laquantite }} ], // Proportion des produits de la catégorie
  @endforeach
]);

var options = {
  title: 'Proportion des produits vendus', // Le titre
  is3D : true, // En 3D
  slices: {
            0: { color: 'gold' },
            1: { color:'DarkKhaki'  },
            2: { color:'PapayaWhip'  }
          }
};

// On crée le chart en indiquant l'élément où le placer "#mon-chart"
var chart = new google.visualization.PieChart(document.getElementById('mon-chart2'));

// On désine le chart avec les données et les options
chart.draw(data, options);
}

//  proportion C.A. par produits par categorie

function drawChart5() {

var data = google.visualization.arrayToDataTable([
  ['Catégories', 'Produits'],
  @foreach ($produits as $produit) // On parcourt les catégories

  [ "{{ $produit->modele }}", {{ $produit->lasomme }} ], // Proportion des produits de la catégorie
  @endforeach
]);

var options = {
  title: 'proportion C.A. par produits', // Le titre
  is3D : true, // En 3D
  slices: {
            0: { color: 'LightSalmon' },
            1: { color:'FireBrick'  },
            2: { color:'brown'  }
          }
};

// On crée le chart en indiquant l'élément où le placer "#mon-chart"
var chart = new google.visualization.PieChart(document.getElementById('mon-chart6'));

// On désine le chart avec les données et les options
chart.draw(data, options);
}


//  proportion type

function drawChart2() {

var data = google.visualization.arrayToDataTable([
  ['Catégories', 'Produits'],
  @foreach ($produitstype as $produit) // On parcourt les catégories

  [ "{{ $produit->modele }}", {{ $produit->laquantite }} ], // Proportion des produits de la catégorie
  @endforeach
]);

var options = {
  title: 'Proportion de ventes des  produits par types', // Le titre
  is3D : true, // En 3D
  slices: {
    0: { color: 'gold' },
            1: { color:'DarkKhaki'  },
            2: { color:'PapayaWhip'  }
          }
};

// On crée le chart en indiquant l'élément où le placer "#mon-chart"
var chart = new google.visualization.PieChart(document.getElementById('mon-chart3'));

// On désine le chart avec les données et les options
chart.draw(data, options);
}


// /!\ PROPORTION PAR PRODUITS PAR TYPE /!\

function drawChart6() {

var data = google.visualization.arrayToDataTable([
  ['Catégories', 'Produits'],
  @foreach ($produitstype as $produit) // On parcourt les catégories

  [ "{{ $produit->modele }}", {{ $produit->lasomme }} ], // Proportion des produits de la catégorie
  @endforeach
]);

var options = {
  title: 'Proportion de ventes des  produits par types', // Le titre
  is3D : true, // En 3D
  slices: {
    0: { color: 'LightSalmon' },
            1: { color:'FireBrick'  },
            2: { color:'brown'  }
          }
};

// On crée le chart en indiquant l'élément où le placer "#mon-chart"
var chart = new google.visualization.PieChart(document.getElementById('mon-chart7'));

// On désine le chart avec les données et les options
chart.draw(data, options);
}

// quatrieme diagramme proportion produit par type

function drawChart3() {

var data = google.visualization.arrayToDataTable([
  ['Catégories', 'Produits'],
  @foreach ($types as $type) // On parcourt les catégories

  [ "{{ $type->nom }}", {{ $type->laquantite }} ], // Proportion des produits de la catégorie
  @endforeach
]);

var options = {
  title: 'Proportion des produits vendus par type', // Le titre
  is3D : true, // En 3D
  slices: {
        0: { color: 'orange' },
        1: { color: 'brown' }
      }
};

// On crée le chart en indiquant l'élément où le placer "#mon-chart"
var chart = new google.visualization.PieChart(document.getElementById('mon-chart4'));

// On désine le chart avec les données et les options
chart.draw(data, options);
}

// /!\ Proportion du C.A. par type /!\

function drawChart7() {

var data = google.visualization.arrayToDataTable([
  ['Catégories', 'Produits'],
  @foreach ($types as $type) // On parcourt les catégories

  [ "{{ $type->nom }}", {{ $type->lasomme }} ], // Proportion des produits de la catégorie
  @endforeach
]);

var options = {
  title: 'Proportion du C.A. par type', // Le titre
  is3D : true, // En 3D
  slices: {

        0: { color: 'blue' },
        1: { color: 'skyblue' }
      }
};

// On crée le chart en indiquant l'élément où le placer "#mon-chart"
var chart = new google.visualization.PieChart(document.getElementById('mon-chart8'));

// On désine le chart avec les données et les options
chart.draw(data, options);
}
</script>
<script type="text/javascript" src="https://www.gstatic.com/charts/loader.js"></script>
<script type="text/javascript">
	// Le code du Chart
</script>

