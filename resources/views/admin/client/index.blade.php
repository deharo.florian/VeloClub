@extends("template")
@section("titre")
Les clients
@endsection
@section("content")
    <div class="container" style="margin-bottom: 9.4rem">
        <h1 class="my-5">Clients</h1>


        <table class="table my-5 table-stripped">
            <thead>
                <tr>
                    <th>N°Client</th>
                    <th>Nom</th>
                    <th>Prenom</th>
                    <th>mail</th>
                    <th>consulter</th>
                </tr>
            </thead>
            <tbody>
                @foreach ($clients as $client )
                <tr>
                    {{-- @dd($uneCommande) --}}
                    <td>{{$client->id}}</td>
                    <td>{{$client->nom_client}}</td>
                    <td>{{$client->prenom}}</td>
                    {{-- <td>
                        <ul>
                            @foreach ($uneCommande->produitCommande as $uneLigne  )
                            <li class="mb-2"><b>{{$uneLigne->nom}}</b> <br>
                            Places : {{$uneLigne->pivot->nbPlaces}}</li>
                            @endforeach
                        </ul>
                    </td> --}}
                    <td>{{$client->email}}</td>
                    <td>
                    <a href="/admin/client/{{ $client->id }}" class="text-danger">Description +</a>
                    </td>
                </tr>
                @endforeach
            </tbody>
        </table>
    </div>
@endsection
