@extends("template")
@section("titre")
Client
@endsection

@section("content")



<h1>{{$client->nom_client}} {{$client->prenom}}</h1>
<h2>N°client {{$client->id}}</h2>
<p class="text-center">
    {{$client->email}}
    <br>{{$client->adresse}}, {{$client->ville}} {{$client->code_postal}}
    <br>date de creation : {{$client->created_at}}
    <br><b>COMMANDES</b>

</p>
<table class="table my-5 table-stripped">
    <thead>
        <tr>
            <th>N°</th>
            <th>Date</th>
            <th>Statut</th>
            {{-- <th>modele</th> --}}
            <th>Prix Total</th>
            <th>factures</th>
        </tr>
    </thead>
    <tbody>
        @foreach ($commandes as $uneCommande )
        <tr>
            {{-- @dd($uneCommande) --}}
            <td>{{$loop->iteration}}</td>
            <td>{{$uneCommande->dateFR($uneCommande->date_commande)}}</td>
            <td>{{$uneCommande->etat}}</td>
            {{-- <td>
                <ul>
                    @foreach ($uneCommande->produitCommande as $uneLigne  )
                    <li class="mb-2"><b>{{$uneLigne->nom}}</b> <br>
                    Places : {{$uneLigne->pivot->nbPlaces}}</li>
                    @endforeach
                </ul>
            </td> --}}
            <td>{{$uneCommande->calcTotal()}}</td>
            <td>
                <form action="/admin/client/pdf" method="POST">
                    @csrf
                <input type="hidden" name="iduser" value="{{$client->id}}">
                <input type="hidden" name="idcommande" value="{{$uneCommande->id}}">
                <input type="submit" value="voir"></form>
            </td>
        </tr>
        @endforeach
    </tbody>
</table>


@endsection
