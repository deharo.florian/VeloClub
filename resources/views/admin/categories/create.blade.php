
@extends("template")
@section("titre")
Produit formulaire
@endsection

@section("content")
{{-- @dd($_POST); --}}
<div class="container my-5">

<div class="divform">

    <h1 class="m-5">Formulaire d'ajout</h1>
    <form method='post' class="m-5" action="/admin/categorie" enctype="multipart/form-data">
        @csrf
        @error('nom')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
            @enderror

            <div class="form-floating mb-3 ">
                <input type="text" class="form-control" id="nom" placeholder="" name="nom" value="{{old("nom")  }}">
          <label for="nom">nom</label>
       </div>
       @error('image_categorie')
          <div class='alert alert-danger mt-1'>{{$message}}</div>
          @enderror
          <div class="input-group mb-3">
              <label class="form-label" for="customFile">Image de la categorie </label>
           <input type="file" name="image_categorie" class="form-control" id="customFile" />
       </div>

       <button class="btn btn-primary">
          Ajouter
       </button>
    </form>
</div>
</div>
{{-- <div class="" style="padding:7%"></div> --}}
@endsection
