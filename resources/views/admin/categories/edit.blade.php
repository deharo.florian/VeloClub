
@extends("template")
@section("titre")
Produit formulaire
@endsection

@section("content")
{{-- @dd($_POST); --}}
<div class="container">
@if(Session::has("message"))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif

    <h1>Formulaire de modification</h1>
    <form method='post' action="/admin/categorie/{{$categorie->id}}" enctype="multipart/form-data">
        @method("put")
            @csrf
            @error('nom')
                <div class='alert alert-danger mt-1'>{{$message}}</div>
            @enderror

       <div class="form-floating mb-3 ">
          <input type="text" class="form-control" id="nom" placeholder="" value="{{old("nom") ?? $categorie->nom }}" name="nom">
          <label for="nom">nom</label>
       </div>
       @error('image_categorie')
          <div class='alert alert-danger mt-1'>{{$message}}</div>
       @enderror

       <div class="input-group mb-3">
           <label class="form-label" for="customFile">Image de la categorie </label>
           <input type="file" name="image_categorie" class="form-control" id="customFile" />
       </div>

       <button class="btn btn-primary">
         Modifier
       </button>
    </form>
</div>

@endsection
