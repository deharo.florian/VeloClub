
@extends("template")
@section("titre")
Produit formulaire
@endsection

@section("content")
{{-- @dd($_POST); --}}
<div class="container">
@if(Session::has("message"))
    <div class="alert alert-success">
        {{Session::get('message')}}
    </div>
@endif

<div class="divform">

    <form method='post' class="p-5" action="/admin/produit" enctype="multipart/form-data">
        <h1>Formulaire d'ajout de produit</h1>
        @csrf
        @error('modele')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
         @enderror

         <div class="form-floating mb-3 ">
             <input type="text" class="form-control" value="{{old("modele")}}" id="modele" placeholder="" name="modele">
          <label for="modele">modele</label>
        </div>
       @error('description')
       <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror

        <div class="form-floating mb-3">
            <textarea class="form-control" placeholder="Leave a comment here" value="{{old("description")}}" id="description" name="description" style="height: 100px"></textarea>
            <label for="description">Description</label>
        </div>

        @error('prixUnitaire')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror
        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="prixUnitaire" value="{{old("prixUnitaire")}}" name="prixUnitaire">
            <label for="prixUnitaire">Prix</label>
        </div>

        @error('estDisponible')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror
        <div class="input-group mb-3">
            <label class="input-group-text" for="estDisponible">disponibilité</label>
            <select class="form-select" id="estDisponible" name="estDisponible">
                <option value="1" selected>Disponible</option>
                <option value="0">indisponible</option>
            </select>
        </div>

        @error('enfant')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror
        <div class="input-group mb-3">
            <label class="input-group-text" for="enfant">Pour</label>
            <select class="form-select" id="enfant" name="enfant">
               <option value="1" selected>enfant</option>
               <option value="0">adulte</option>
           </select>
       </div>

       @error('categories_id')
       <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
    <div class="input-group mb-3">
           <label class="input-group-text" for="categories_id">Categorie</label>
           <select class="form-select" name="categories_id" id="categories_id">
               <option value="1" selected>BMX</option>
               <option value="2">Velo de course</option>
               <option value="3">Velo de ville</option>
               <option value="4">VTT</option>
            </select>
        </div>

        @error('types_id')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror
        <div class="input-group mb-3">
            <select name="types_id" class="form-select">
                <option value="1">velo</option>
                <option value="2">accessoire</option>
            </select>
        </div>

        @error('image')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror
        <div class="input-group mb-3">
            <label class="form-label" for="customFile">Image du produit </label>
            <input type="file" name="image" class="form-control" id="customFile" />
        </div>

        <button class="btn btn-primary">
            Ajouter
        </button>
    </form>
</div>
</div>

@endsection
