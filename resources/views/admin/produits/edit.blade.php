
@extends("template")
@section("titre")
Produit formulaire Modification
@endsection

@section("content")

{{-- @dd($_POST); --}}
<div class="container">
    <div class="divform">

        <form method='post' class="p-5" action="/admin/produit/{{$produit->id}}" enctype="multipart/form-data">
            <h1>Formulaire de modification </h1>
            @method("put")
            @csrf
            <input type="hidden" name="id" value='{{$produit->id}}'>
            @error('modele')
            <div class='alert alert-danger mt-1'>{{$message}}</div>
            @enderror
       <div class="form-floating mb-3 ">
          <input type="text" value='{{old("modele") ?? $produit->modele}}' class="form-control" id="modele" placeholder='' name="modele">
          <label for="modele">modele</label>
        </div>
        @error('description')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror

        <div class="form-floating mb-3">
           <textarea class="form-control" placeholder="Leave a comment here"  id="description" name="description" style="height: 100px">{{old("description") ?? $produit->description}}</textarea>
           <label for="description">Description </label>
        </div>
        @error('prixUnitaire')
        <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror

        <div class="form-floating mb-3">
            <input type="text" class="form-control" id="prixUnitaire" value='{{old("prixUnitaire") ?? $produit->prixUnitaire}}' name="prixUnitaire">
            <label for="prixUnitaire">Prix</label>
        </div>
        @error('estDisponible')
          <div class='alert alert-danger mt-1'>{{$message}}</div>
          @enderror
          <div class="input-group mb-3">
           <label class="input-group-text" for="estDisponible">disponibilité</label>
           <select class="form-select" id="estDisponible" name="estDisponible">
            @if ($produit->enfant == 1)
            <option value="1" selected>Disponible</option>
            <option value="0">indisponible</option>

            @else
            <option value="1" >Disponible</option>
            <option value="0" selected>indisponible</option>

            @endif
        </select>
    </div>
    @error('enfant')
    <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
    <div class="input-group mb-3">
        <label class="input-group-text" for="enfant">Pour</label>
        <select class="form-select" id="enfant" name="enfant">
            <option value="1" selected>enfant</option>
            <option value="0">adulte</option>
        </select>
    </div>
    @error('categories_id')
    <div class='alert alert-danger mt-1'>{{$message}}</div>
    @enderror
    <div class="input-group mb-3">
        <label class="input-group-text" for="categories_id">Categorie</label>
        <select class="form-select" name="categories_id" id="categories_id">
            <option value="1" selected>BMX</option>
            <option value="2">Velo de course</option>
            <option value="3">Velo de ville</option>
            <option value="4">VTT</option>
        </select>
    </div>
    @error('types_id')
                    <div class='alert alert-danger mt-1'>{{$message}}</div>
        @enderror

        <div class="input-group mb-3">
            <select name="types_id" class="form-select">
                <option value="1">velo</option>
                <option value="2">accessoire</option>
       </select>
       </div>

       <div class="input-group mb-3">
           <label class="form-label" for="customFile">Image du produit </label>
           <input type="file" name="image" value='{{$produit->image}}' class="form-control" id="customFile" />
        </div>
        @error('image')
                    <div class='alert alert-danger mt-1'>{{$message}}</div>
                @enderror


                <button class="btn btn-primary">
                    Modifier
                </button>
            </form>
        </div>
</div>

@endsection
