<nav
    class="d-flex grise  justify-content-center text-center text-lg-start justify-content-lg-end  navbar navbar-expand-lg navbar-dark">

    <div>

        <button class="navbar-toggler" type="button" data-bs-toggle="collapse" data-bs-target="#navbarSupportedContent"
            aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
            <span class="navbar-toggler-icon"></span>
        </button>
        <div class="collapse navbar-collapse" id="navbarSupportedContent">
            <ul class="navbar-nav me-auto mb-2 mb-lg-0">
                <li class="nav-item">
                    <a class="nav-link " aria-current="page" href="/">Accueil</a>
                </li>
                <li class="nav-item">
                    <a href="/produit" class="nav-link">Produit</a>
                </li>
                @guest
                <li class="nav-item">
                    <a class="nav-link" href="{{ route('register') }}">Inscription</a>
                </li>
                    @if (Route::has('login'))
                        <li class="nav-item">
                            <a class="nav-link" href="{{ route('login') }}">{{ __('Login') }}</a>
                        </li>
                    @endif
                @else
                    @if (auth()->user() && auth()->user()->role->nom == 'client')
                        <li class="nav-item">
                            <a href="/client/panier" class="nav-link">Panier</a>
                        </li>
                        <li class="nav-item">
                            <a href="/client/commandes" class="nav-link">Commande</a>
                        </li>

                    @elseif (auth()->user() && auth()->user()->role->nom == 'admin')
                    <li class="nav-item">
                        <a href="/categorie" class="nav-link">Categorie</a>
                    </li>
                    <li class="nav-item">
                        <a href="/admin/client" class="nav-link">Client</a>
                    </li>
                    <li class="nav-item">
                        <a href="/diagram" class="nav-link">Statistiques</a>
                    </li>
                    @endif

                        <li class="nav-item">
                    <form id="logout-form" action="{{ route('logout') }}" method="POST">
                        @csrf
                        <button type="submit" class="nav-link btn btn-link" style="text-decoration: none">{{ __('Logout') }}</button>
                    </form>
                        </li>
                @endguest

                {{-- <li class="nav-item">
                    <a class="nav-link" href="#">Logout</a>
                </li> --}}

            </ul>
        </div>
    </div>
</nav>
