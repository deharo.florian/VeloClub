@extends('template')

@section('content')
    {{-- <div class="container">
        <div class="row justify-content-center">
            <div class="col-md-8">
                <div class="card">
                    <div class="card-header">{{ __('Register') }}</div>

                    <div class="card-body">
                        <form method="POST" action="{{ route('register') }}">
                            @csrf

                            <div class="row mb-3">
                                <label for="name" class="col-md-4 col-form-label text-md-end">{{ __('Name') }}</label>

                                <div class="col-md-6">
                                    <input id="name" type="text" class="form-control @error('name') is-invalid @enderror"
                                        name="name" value="{{ old('name') }}" required autocomplete="name" autofocus>

                                    @error('name')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="email"
                                    class="col-md-4 col-form-label text-md-end">{{ __('Email Address') }}</label>

                                <div class="col-md-6">
                                    <input id="email" type="email" class="form-control @error('email') is-invalid @enderror"
                                        name="email" value="{{ old('email') }}" required autocomplete="email">

                                    @error('email')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password"
                                    class="col-md-4 col-form-label text-md-end">{{ __('Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password" type="password"
                                        class="form-control @error('password') is-invalid @enderror" name="password"
                                        required autocomplete="new-password">

                                    @error('password')
                                        <span class="invalid-feedback" role="alert">
                                            <strong>{{ $message }}</strong>
                                        </span>
                                    @enderror
                                </div>
                            </div>

                            <div class="row mb-3">
                                <label for="password-confirm"
                                    class="col-md-4 col-form-label text-md-end">{{ __('Confirm Password') }}</label>

                                <div class="col-md-6">
                                    <input id="password-confirm" type="password" class="form-control"
                                        name="password_confirmation" required autocomplete="new-password">
                                </div>
                            </div>

                            <div class="row mb-0">
                                <div class="col-md-6 offset-md-4">
                                    <button type="submit" class="btn btn-primary">
                                        {{ __('Register') }}
                                    </button>
                                </div>
                            </div>
                        </form>
                    </div>
                </div>
            </div>
        </div>
    </div> --}}
    {{-- ligne --}}

    <div class="container">


        <form method="POST" action="{{ route('register') }}">
            @csrf
            <div class="divform p-5">
                <h1 class='text-center'><span class='badge rounded-pill bg-warning text-dark mb-4'>Inscription</span></h1>

                <input type="text" required
                    class="form-control form-control-lg mb-3 @error('nom_client') is-invalid @enderror" name="nom_client"
                    placeholder="Nom" value="{{ old('nom_client') }}" required autocomplete="nom_client" autofocus>

                    @error('nom_client')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <input type="text" required class="form-control form-control-lg mb-3 @error('prenom') is-invalid @enderror"
                    name="prenom" placeholder="prenom" value="{{ old('prenom') }}">
                    @error('prenom')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror

                <input id="email" type="email" placeholder="Email" class="form-control  form-control-lg mb-3 @error('email') is-invalid @enderror" name="email"
                    value="{{ old('email') }}" required autocomplete="email">

                @error('email')
                    <span class="invalid-feedback" role="alert">
                        <strong>{{ $message }}</strong>
                    </span>
                @enderror
                <input id="password" type="password"
                class="form-control form-control-lg mb-3 @error('password') is-invalid @enderror" name="password" placeholder="mot de passe"
                required autocomplete="new-password">

            @error('password')
                <span class="invalid-feedback" role="alert">
                    <strong>{{ $message }}</strong>
                </span>
            @enderror

                <input id="password-confirm" type="password" class="form-control form-control-lg mb-3"
                    name="password_confirmation" placeholder="mot de passe confirmation" required autocomplete="new-password">
                <input type="text" required class="form-control form-control-lg mb-3" name="adresse" placeholder="adresse"
                    value="{{ old('adresse') }}">
                <input type="text" required class="form-control form-control-lg mb-3" name="code_postal"
                    value="{{ old('code_postal') }}" placeholder="code postal">
                <input type="text" required class="form-control form-control-lg mb-3" name="ville" placeholder="ville"
                    value="{{ old('ville') }}">

                <input type="submit" class="form-control form-control-lg mb-3 btn btn-outline-primary" value="s'enregistrer">
            </div>
        </form>
    </div>
@endsection
