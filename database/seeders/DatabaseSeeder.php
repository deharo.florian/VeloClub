<?php

namespace Database\Seeders;

use App\Models\Type;
use App\Models\Categorie;
use App\Models\Produit;
use App\Models\ProduitCommande;
use App\Models\User;
use App\Models\Role;
use App\Models\Commande;
use App\Models\Commentaire;

use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;


class DatabaseSeeder extends Seeder
{
    /**
     * Seed the application's database.
     *
     * @return void
     */
    public function run()
    {
        // \App\Models\User::factory(10)->create();
        $type1 = Type::create([
            "nom"=>"Vélo",
        ]);
        $type2 = Type::create([
            "nom"=>"Accessoire",
        ]);

        $categorie1 = Categorie::create([
            "nom"=>"BMX",
            "image_categorie"=>"categorie/velo_bmx.jpg"
        ]);
        $categorie2 = Categorie::create([
            "nom"=>"Vélo de course",
            "image_categorie"=>"categorie/velo_de_course.jpg"
        ]);
        $categorie3 = Categorie::create([
            "nom"=>"Vélo de ville",
            "image_categorie"=>"categorie/velo_ville.jpg"
        ]);
        $categorie4 = Categorie::create([
            "nom"=>"VTT",
            "image_categorie"=>"categorie/vtt.jpg"
        ]);

        $produit1 = Produit::create([
            "modele"=>"rockrider VTT ST 100 GRIS 27,5",
            "description"=>"Ce VTT  est conçu pour vos 1ères randonnées VTT, par temps sec. Efficacité ? Robustesse ? Les 2 s'il vous plaît ! Franchissez sans peine et sans casse vos premiers obstacles : cadre aluminium léger et roues en 27,5' montées sur jantes double parois.",
            "prixUnitaire"=>259,
            "estDisponible"=>true,
            "enfant"=>false,
            "categories_id"=>4,
            "types_id"=>1,
            "image"=>"produit/rockrider.jpg"
        ]);

        $admin = Role::create(["nom"=>"admin"]);
        $client = Role::create(["nom"=>"client"]);


        $user1=User::create([
            "nom_client"=>"test",
            "prenom"=>"florian",

            "password"=>bcrypt("florian@mail.com1"),
            "email"=>"florian@mail.com",
            "adresse"=>"1 rue du test",
            "ville"=>"Gotham",
            "code_postal"=>"99999",
            "roles_id"=>1
        ]);

        $user2 = User::create([
            "nom_client"=>"test",
            "prenom"=>"david",

            "password"=>bcrypt("david@mail.com1"),
            "email"=>"david@mail.com",
            "adresse"=>"2 rue du test",
            "ville"=>"Gotham",
            "code_postal"=>"10101",
            "roles_id"=>2
        ]);

        $commande1 = Commande::create([
            "date_commande"=>"2022-05-11",
            "date_livraison"=>null,
            "etat"=>"en preparation",
            "utilisateurs_id"=>2
        ]);


      ProduitCommande::create([
            "produit_id"=>$produit1->id,
            "commande_id"=>$commande1->id,
            "quantite"=>3
        ]);

        Commentaire::create([
            "produit_id"=>1,
            "utilisateur_id"=>1,
            "texte"=>"Super vélo !",
            "note"=>3
        ]);






    }
}
