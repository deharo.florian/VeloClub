<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produits', function (Blueprint $table) {
            $table->id();
            $table->string("modele");
            $table->string('description',400);
            $table->float('prixUnitaire');
            $table->boolean('estDisponible');
            $table->boolean('enfant');
            $table->string('image');
            $table->foreignId("categories_id")->constrained("categories","id")->onDelete("cascade");
            $table->foreignId("types_id")->constrained("types","id")->onDelete("cascade");
            $table->timestamps();
        });
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produits');
    }
};
