<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('produit_commande', function (Blueprint $table) {
            $table->foreignId("produit_id")->constrained("produits","id")->onDelete("cascade");
            $table->foreignId("commande_id")->constrained("commandes","id")->onDelete("cascade");
            $table->integer("quantite");
            $table->primary(["produit_id","commande_id"]);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('produit_commande');
    }
};
