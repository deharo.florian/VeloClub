<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

return new class extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('utilisateurs', function (Blueprint $table) {
            $table->id();
            // $table->string("nom_client",50);
            // $table->string("prenom",50);
            // $table->string("email",100)->unique();
            // $table->string("mdp");
            // $table->string("adresse");
            // $table->string("ville",100);
            // $table->string("code_postal",5);
            // $table->foreignId("roles_id")->constrained("roles","id")->onDelete("cascade");
            // $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('utilisateurs');
    }
};
