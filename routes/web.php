<?php

use Illuminate\Support\Facades\Route;
use App\Models\Categorie ;
use App\Http\Controllers\CategorieController ;
use App\Http\Controllers\ProduitController ;
use App\Http\Controllers\CommandeController ;
use Illuminate\Support\Facades\Auth;
use App\Http\Controllers\Admin\AdminProduitsController;
use App\Http\Controllers\Admin\AdminCategorieController;
use App\Http\Controllers\Admin\AdminClientController;
use App\Http\Controllers\Admin\AdminStatController;
use App\Http\Controllers\CaptchaController;

use App\Http\Controllers\Client\ClientPanierController;
use App\Http\Controllers\Client\ClientCommandeController;
use App\Http\Controllers\Client\ClientCommentaireController;
use App\Http\Controllers\Admin\DiagramController;
// use PDF;


/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    $lesCategories = Categorie::all();
    return view('accueil',["lesCategories"=>$lesCategories]);
});

// Route::get('/categorie',[CategorieController::class,"index"]);

// Route::get('/categorie/create',[CategorieController::class,"create"]);


// Route::post('/categorie',[CategorieController::class,"store"]);

// Route::get('/categorie/{categorie}',[CategorieController::class,"show"]);

// Route::get('/categorie/{categorie}/edit',[CategorieController::class,"edit"]);

// Route::put('/categorie/{categorie}',[CategorieController::class,"update"]);

// Route::delete('/categorie/{categorie}',[CategorieController::class,"destroy"]);

Route::resource("/categorie",CategorieController::class)->parameters(["categorie"=>"categorie"]);

Route::resource("/produit",ProduitController::class)->parameters(["produit"=>"produit"]);

Route::resource("/commande",CommandeController::class)->parameters(["commande"=>"commande"]);

Auth::routes();

Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');

Route::resource("/admin/produit",AdminProduitsController::class)->parameters(["produit"=>"produit"])->middleware("estAdmin")->name('index','produit');



Route::resource("/admin/categorie",AdminCategorieController::class)->parameters(["categorie"=>"categorie"])->middleware("estAdmin");

Route::resource("/admin/client",AdminClientController::class)->parameters(["client"=>"client"])->middleware("estAdmin");

Route::post("/admin/client/pdf",[AdminClientController::class, 'affichePDF'])->middleware("estAdmin");



Route::middleware("auth")->group(
    function(){

//Les routes du panier
Route::resource("client/panier",ClientPanierController::class)->parameters(["panier"=>"panier"]);
Route::delete("client/panier",[ClientPanierController::class,"vider"]);
Route::get("/pdf",[ClientPanierController::class,"createPDF"]);
// Route::get("/client/pdf",[ClientPanierController::class,"affichePDF"]);



//Les routes des commandes (pour les clients)
Route::resource("client/commandes",ClientCommandeController::class)->parameters(["commande"=>"commande"])->only(["index","show","store"]);
Route::get('/client/pdf', [ClientCommandeController::class, 'affichePDF']);
}
);

// route de commentaire
Route::resource("client/commentaires",ClientCommentaireController::class)->parameters(["commentaire"=>"commentaire"])->except(["show"]);

Route::get("/contact",[App\Http\Controllers\HomeController::class,"formulaireContact"]);
Route::post("/contact",[App\Http\Controllers\HomeController::class,"traitementContact"]);


Route::get('addcaptcha',[CaptchaController::class, 'addcaptcha'])->name('addcaptcha');
Route::post('addcaptcha',[CaptchaController::class, 'addcaptchapost'])->name('addcaptcha.post');
Route::get('refreshcaptcha',[App\Http\Controllers\HomeController::class, 'refreshCaptcha'])->name('refreshcaptcha');

// Route::resource("client/panier",ClientPanierController::class)->parameters(["panier"=>"panier"])->middleware("auth");
// Route::delete("client/panier",[ClientPanierController::class,"vider"])->middleware("auth");

Route::get("diagram", [DiagramController::class, "diagram"])->name("diagram")->middleware("estAdmin");
