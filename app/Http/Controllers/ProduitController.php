<?php

namespace App\Http\Controllers;

use App\Http\Controllers\Controller;
use App\Models\Produit;
use App\Models\Categorie;
use App\Models\Type;
use Illuminate\Http\Request;

class ProduitController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index(Request $request)
    {
        $querry=Produit::with(["categories","types"]);
        if($request->input("categories")){
            $querry=$querry->where("categories_id","=",$request->input("categories"));
        }
        if($request->input("types")){
            $querry=$querry->where("types_id","=",$request->input("types"));
        }
        if($request->input("enfant")){
            $querry=$querry->where("enfant","=",$request->input("enfant"));
        }
        if($request->input("modele")){
            $querry=$querry->where("modele","like","%".$request->input("modele")."%");
        }
        $produits = $querry->paginate(8);
        $type = Type::all();
        $categorie = Categorie::all();
        return view("produits.index",["lesProduits"=>$produits,"lesCategories"=>$categorie,"lesTypes"=>$type]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
    //     //
    //     return view('produits.create');
    }

    // /**
    //  * Store a newly created resource in storage.
    //  *
    //  * @param  \Illuminate\Http\Request  $request
    //  * @return \Illuminate\Http\Response
    //  */
    public function store(Request $request)
    {


    //     $attributes=$request->validate([
    //         "modele"=>"required|min:3|max:255|unique:produits,modele",
    //         "description"=>"required | min:3|max:400",
    //         "prixUnitaire"=>"required",
    //         "estDisponible"=>"required",
    //         "enfant"=>"required",
    //         "image"=>"required",
    //         "categories_id"=>"required",
    //         "types_id"=>"required",
    //     ]);


    //     $nomImage = $request->file("image")->getClientOriginalName();

    //     $attributes["image"]=$request->file("image")->storeAs("produit",$nomImage,"public");


    //     Produit::create($attributes);

    //     // session()->flash("success","Le produit a bien été ajouté");


    //     return redirect("/produit"); //->with('message','Le produit  a été ajouté');

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        //
        return view("produits.show",["produit"=>$produit]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
    //     //
    //     return view("produits.edit",["produit"=>$produit]);



    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $produit)
    {
    //     $attributes=$request->validate([
    //         "modele"=>"required|min:3|max:255",
    //         "description"=>"required",
    //         "prixUnitaire"=>"required",
    //         "estDisponible"=>"required",
    //         "enfant"=>"required",
    //         "image"=>"image",
    //         "categories_id"=>"required",
    //         "types_id"=>"required",
    //     ]);



    //     if(isset($attributes["image"])){
    //         $nomImage = $request->file("image")->getClientOriginalName();

    //         $attributes["image"]=$request->file("image")->storeAs("produit",$nomImage,"public");
    //     }


    //     $produit->update($attributes);

    //     session()->flash("success","Le produit a bien été modifié");

    //     return redirect("/produit");//->with('message','Le produit a été modifié');
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Produit  $produit
     * @return \Illuminate\Http\Response
     */

}
