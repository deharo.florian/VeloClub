<?php

namespace App\Http\Controllers;

use App\Mail\Contact;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Mail;

class HomeController extends Controller
{
    /**
     * Create a new controller instance.
     *
     * @return void
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    /**
     * Show the application dashboard.
     *
     * @return \Illuminate\Contracts\Support\Renderable
     */
    public function index()
    {
        return view('home');
    }

    public function formulaireContact(Request $request){
        return view("client.contact");
    }

    /**
     * Traitement du formulaire de contacte
     *
     * @param Request $request
     * @return void
     */
    public function traitementContact(Request $request){
        $attributs=$request->validate([
            "email"=>"required|email",
            "sujet"=>"required|min:2|max:255",
            "texte"=>"required|max:255"
        ]);

        Mail::to('deharo.florian@gmail.com')->send(new Contact($attributs["email"],$attributs["sujet"],$attributs["texte"]));
        session()->flash("mail","Votre message a bien été envoyé");
        return view("client.contact");

    }

    public function refreshCaptcha()
    {
        return response()->json(['captcha'=> captcha_img()]);
    }


}
