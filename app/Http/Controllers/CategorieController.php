<?php

namespace App\Http\Controllers;

use App\Models\Categorie;
use Illuminate\Http\Request;

class CategorieController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    //Je retourne la vue pays.blade.php en transmettant $lesPays associer a la chaine dataPays
    return view("categories.categorie",["lesCateg"=>Categorie::get()]);

    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    // public function create()
    // {
    //     //
    //         return view("categories.create");

    // }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    // public function store(Request $request)
    // {
    //     $attributes = $request->validate([
    //         "nom"=>"required|unique:categories,nom",
    //         "image_categorie"=>"image"
    //     ]);
    //     $nomImage = $request->file("image_categorie")->getClientOriginalName();

    //     $attributes["image_categorie"]=$request->file("image_categorie")->storeAs("categorie",$nomImage,"public");


    //     Categorie::create($attributes);
    //     return redirect("/categorie");
    // }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    public function show(Categorie $categorie)
    {
        return view("categories.show",["categorie"=>$categorie]);

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    // public function edit(Categorie $categorie)
    // {
    //     //
    //     return view("categories.edit",["categorie"=>$categorie]);
    // }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    // public function update(Request $request, Categorie $categorie)
    // {
    //     //
    //     $attributes = $request->validate([
    //         "nom"=>"required|unique:categories,nom"

    //     ]);
    //     $categorie->update($attributes);
    //     return redirect("/categorie");

    // }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\Categorie  $categorie
     * @return \Illuminate\Http\Response
     */
    // public function destroy(Categorie $categorie)
    // {
    //     //
    //     $categorie->delete();
    //     return redirect("/categorie");

    // }
}
