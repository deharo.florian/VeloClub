<?php

namespace App\Http\Controllers\Client;

use App\Models\Commentaire;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class ClientCommentaireController extends Controller
{

    public function store(Request $request)
    {
        $attributs=$request->validate([
            "texte"=>"required|min:4|max:255",
            "produit_id"=>"required|exists:produits,id",
            "note"=>"required|min:1|max:5"
        ]);

        $utilisateur=auth()->user();

        $attributs["utilisateur_id"]=$utilisateur->id;
        // dd($attributs);
        Commentaire::create($attributs);

        session()->flash("success","Commentaire ajouter");
        return redirect()->back();
    }

    public function moyenneNote($note){
        
    }

}
