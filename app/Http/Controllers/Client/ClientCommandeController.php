<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Darryldecode\Cart\Facades\CartFacade;
use App\Models\Commande;
use App\Models\ProduitCommande;
use App\Models\User;

use PDF;



class ClientCommandeController extends Controller
{

    public function index()
    {
        $utilisateur=auth()->user();
        //Recupere les commandes d'un utilisateur
        $commandes=Commande::select("*")->where("utilisateurs_id","=",$utilisateur->id)->get();
        return view("client.commandes.index",["commandes"=>$commandes]);
    }



    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //TODO Validation
        $panier = CartFacade::getContent();
        $commande=Commande::create([
            "utilisateurs_id"=>auth()->user()->id,
            "date_commande"=>date('Y-m-d', time()),
            "etat"=>"en préparation",
        ]);
        foreach($panier as $uneLigne){
            ProduitCommande::create([
                "commande_id"=>$commande->id,
                "produit_id"=>$uneLigne->id,
                "quantite"=>$uneLigne->quantity
            ]);
        }
        CartFacade::clear();
        session()->flash("success","La commande est enregistrée");
        return redirect("/client/pdf");
    }


    public function affichePDF(Request $request) {
        $utilisateur=auth()->user();

        if(isset($request->id)){

            $commandes = Commande::select("*")->where("utilisateurs_id","=",$utilisateur->id)->where("id","=",$request->id)->get();
            $produits = ProduitCommande::select("*")->join('produits', 'produits.id', '=', 'produit_commande.produit_id')->where("commande_id","=",$request->id)->get();
        }else{
            $commandes = Commande::select("*")->where("utilisateurs_id","=",$utilisateur->id)->orderby('id', 'desc')->limit("1")->get();
            $produits = ProduitCommande::select("*")->join('produits', 'produits.id', '=', 'produit_commande.produit_id')->where("commande_id","=",$commandes["0"]["id"])->get();

        }
        // $commandes = Commande::select("*")->where("commandes_id","=",$utilisateur->id)->get()->toArray();
        foreach($commandes as $commande){

            view()->share('commande',$commande);
        }
        view()->share('utilisateur',$utilisateur);
        view()->share('produits',$produits);

        $pdf = PDF::loadView('pdf');
        return $pdf->download('pdf_file.pdf');
      }

}
