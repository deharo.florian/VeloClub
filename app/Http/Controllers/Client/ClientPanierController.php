<?php

namespace App\Http\Controllers\Client;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Darryldecode\Cart\Facades\CartFacade;
use App\Models\Produit;
use PDF;


class ClientPanierController extends Controller
{
    public function index()
    {
        $panier = CartFacade::getContent();
        return view("client.panier.index", ["panier" => $panier]);
    }

    public function store(Request $request)
    {

        $attributs = $request->validate([
            "idProduit" => "required|exists:produits,id",
            "nb" => "required|min:1|max:20"
        ]);

        $idUser = auth()->user()->id;

        $produit = Produit::findOrFail($request->idProduit);
        //Si la destination est deja dans le panier
        if (CartFacade::get($produit->id) !== null) {
            //On ajuster la quantite
            CartFacade::update($produit->id, [
                'quantity' => $request->nb,
            ]);
        } else {
            //Sinon on ajoute la destination au panier
            CartFacade::add(array(
                'id' => $produit->id,
                'name' => $produit->modele,
                'price' => $produit->prixUnitaire,
                'quantity' => $request->nb,
                'attributes' => array(),
                'associatedModel' => $produit
            ));

        }
        session()->flash("success","Ajout de ".$produit->modele." dans votre panier");
        return redirect("client/panier");
    }

    public function update(Request $request, Produit $panier)
    {

        $request->validate([
            "nb" => "required|numeric|min:0|max:20"
        ]);
        if ($request->nb <= 0) {
            CartFacade::remove($panier->id);
        } else {
            CartFacade::update($panier->id, ["quantity" => [
                "value" => $request->nb,
                "relative" => false
            ]]);

        }
        session()->flash("success","Modification de votre panier");
        return redirect("/client/panier");
    }


    public function destroy(Produit $panier){
        CartFacade::remove($panier->id);
        session()->flash("success","Un produit a été supprimer");

        return redirect("/client/panier");
    }


    public function vider(){
        CartFacade::clear();
        session()->flash("success","Votre panier est vide");

        return redirect("/client/panier");
    }

    public function createPDF() {
        $utilisateur=auth()->user();
        $CartItems = \Cart::getContent()->toArray();
        view()->share('CartItems',$CartItems);
        view()->share('utilisateur',$utilisateur);
        $pdf = PDF::loadView('pdf2');
        return $pdf->download('pdf_file.pdf');
      }


}
