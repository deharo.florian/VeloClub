<?php

namespace App\Http\Controllers\Admin;

use App\Models\ProduitCommande;
use App\Models\User;

use PDF;
use Illuminate\Http\Request;
use App\Http\Controllers\Controller;
use App\Models\Commande;

class AdminClientController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $clients = User::where("roles_id","=",2)->get();
        return view("admin.client.index",["clients"=>$clients]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {

    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $client)
    {
        $commandes=Commande::select("*")->where("utilisateurs_id","=",$client->id)->get();
if($client->roles_id == 2){
    return view("admin.client.show",["client"=>$client,"commandes"=>$commandes]);
}else{
    abort(404);
}

    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }
    public function affichePDF(Request $request) {
        // $utilisateur=auth()->user();

        $utilisateur = User::where("id","=",$request->iduser)->get();
        $commandes = Commande::select("*")->where("utilisateurs_id","=",$request->iduser)->where("id","=",$request->idcommande)->get();
        $produits = ProduitCommande::select("*")->join('produits', 'produits.id', '=', 'produit_commande.produit_id')->where("commande_id","=",$request->idcommande)->get();
        // $commandes = Commande::select("*")->where("commandes_id","=",$utilisateur->id)->get()->toArray();



        view()->share('commande',$commandes[0]);
        view()->share('utilisateur',$utilisateur[0]);
        view()->share('produits',$produits);

        $pdf = PDF::loadView('pdf');
        return $pdf->download('pdf_file.pdf');
      }

}
