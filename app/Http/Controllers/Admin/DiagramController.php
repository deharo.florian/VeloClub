<?php

namespace App\Http\Controllers\Admin;

use Illuminate\Http\Request;
use App\Models\Categorie;
use App\Models\Type;

use Illuminate\Support\Facades\DB;
use App\Http\Controllers\Controller;

class DiagramController extends Controller
{
	// L'action de la route "diagram"
	public function diagram (Request $request) {
    //Selectionne tous les modeles, le chiffre d'affaire et la quantite de chaque produit vendus par categorie
    // $categories = Categorie::with(['categories'])->get();
    $categories = DB::select(" SELECT categories.id,nom,sum(quantite) as laquantite,sum(produits.prixUnitaire) as lasomme FROM categories inner join produits on categories.id = produits.categories_id inner join produit_commande on produit_commande.produit_id = produits.id GROUP BY categories.id order by sum(quantite) desc ;");
    $categOk = "non";
    if($request->input("categ")){
        $categ = $request->input("categ");
    $produits = DB::select("SELECT modele,sum(quantite) as laquantite,sum(produits.prixUnitaire) as lasomme FROM produits inner join produit_commande on produit_commande.produit_id = produits.id where categories_id = $categ GROUP BY modele order by sum(quantite) desc; ");
        $categOk = "ok";
    }
    else
    {
        $produits = DB::select("SELECT modele,sum(quantite) as laquantite,sum(produits.prixUnitaire) as lasomme FROM produits inner join produit_commande on produit_commande.produit_id = produits.id GROUP BY modele order by sum(quantite) desc; ");
    }
    $types = DB::select(" SELECT types.id,nom,sum(quantite) as laquantite,sum(produits.prixUnitaire) as lasomme FROM types inner join produits on types.id = produits.types_id inner join produit_commande on produit_commande.produit_id = produits.id GROUP BY types.id order by sum(quantite) desc ;");

     $toutType = Type::get();

     $idtypesOk = "non";

    if($request->input("types")){
        $idtypesOk = "ok";
        $idtypes = $request->input("types");
    $produitstype = DB::select("SELECT modele,sum(quantite) as laquantite,sum(produits.prixUnitaire) as lasomme FROM produits inner join produit_commande on produit_commande.produit_id = produits.id where types_id = $idtypes GROUP BY modele order by sum(quantite) desc; ");
    }
    else
    {
        $produitstype = DB::select("SELECT modele,sum(quantite) as laquantite,sum(produits.prixUnitaire) as lasomme FROM produits inner join produit_commande on produit_commande.produit_id = produits.id GROUP BY modele order by sum(quantite) desc; ");
    }
		// La vue "diagram"
		return view("admin.diagram", compact('categories','produits','produitstype'),["toutType"=>$toutType, "types"=>$types,"categOk"=>$categOk,"idtypesOk"=>$idtypesOk]);
	}

}
