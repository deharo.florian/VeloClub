<?php

namespace App\Http\Controllers\Admin;


use App\Models\Produit;
use App\Models\Type;
use App\Models\Categorie;

use Illuminate\Http\Request;
use App\Http\Controllers\Controller;

class AdminProduitsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        return view("produits.index",["lesProduits"=>Produit::get()]);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('admin.produits.create');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */

    public function store(Request $request)
    {
        $attributes=$request->validate([
            "modele"=>"required|min:3|max:255|unique:produits,modele",
            "description"=>"required | min:3|max:400",
            "prixUnitaire"=>"required",
            "estDisponible"=>"required",
            "enfant"=>"required",
            "image"=>"required",
            "categories_id"=>"required",
            "types_id"=>"required",
        ]);

        $nomImage = $request->file("image")->getClientOriginalName();
        $attributes["image"]=$request->file("image")->storeAs("produit",$nomImage,"public");

        Produit::create($attributes);
        return redirect("/produit");

    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show(Produit $produit)
    {
        //
        return view("admin.produits.show",["produit"=>$produit]);

    }
    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit(Produit $produit)
    {
        return view("admin.produits.edit",["produit"=>$produit]);
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, Produit $produit)
    {
        $attributes=$request->validate([
            "modele"=>"required|min:3|max:255|unique:produits,modele",
            "description"=>"required",
            "prixUnitaire"=>"required",
            "estDisponible"=>"required",
            "enfant"=>"required",
            "image"=>"image",
            "categories_id"=>"required",
            "types_id"=>"required",
        ]);

        if(isset($attributes["image"])){
            $nomImage = $request->file("image")->getClientOriginalName();
            $attributes["image"]=$request->file("image")->storeAs("produit",$nomImage,"public");
        }

        $produit->update($attributes);
        return redirect("/produit");
        }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy(Produit $produit)
    {
        //
        $produit->delete();
        return redirect('/produit');
    }
}
