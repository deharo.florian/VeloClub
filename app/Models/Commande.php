<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commande extends Model
{
    use HasFactory;
    protected $guarded=["id"];

    public function produitCommande(){
        return $this->belongsToMany(Produit::class,"produit_commande")->using(ProduitCommande::class)->withPivot("quantite")->withTimestamps();
    }

    public function calcTotal(){
        $resultat=0;
        foreach($this->produitCommande as $uneLigne){
            $resultat=$uneLigne->prixUnitaire*$uneLigne->pivot->quantite;
        }
        return $resultat;
    }

    public static function dateFR($date){
        $date = explode('-', $date, 3);
         $datefr = $date[2]."/".$date[1]."/".$date[0] ;
         return $datefr;
       }


}
