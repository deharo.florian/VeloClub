<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Commentaire extends Model
{
    use HasFactory;

    protected $guarded=["id"];

    public function autheur(){
        return $this->belongsTo(User::class,"utilisateur_id");
    }

    public function produit(){
        return $this->belongsTo(Produit::class,"produit_id");
    }

}
