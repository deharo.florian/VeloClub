<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Categorie extends Model
{
    use HasFactory;
    protected $guarded=[];

    public function categories(){
        return $this->hasMany(Produit::class, 'categories_id');
        }

public function produits(){
        return $this->hasMany(Produit::class, 'categories_id');

    }


}
