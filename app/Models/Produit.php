<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Produit extends Model
{
    use HasFactory;
    protected $guarded=["id"];

    public function produitCommande(){
        return $this->belongsToMany(Commande::class,"produit_commande")->using(ProduitCommande::class)->withPivot("quantite")->withTimestamps();
    }

    public function commentaires(){
        return $this->hasMany(Commentaire::class);
    }

    public function categories()
    {
        return $this->belongsTo(Categorie::class, 'categories_id');
    }

    public function types()
    {
        return $this->belongsTo(Type::class);
    }

}
