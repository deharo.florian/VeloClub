<?php

namespace App\Mail;

use Illuminate\Bus\Queueable;
use Illuminate\Contracts\Queue\ShouldQueue;
use Illuminate\Mail\Mailable;
use Illuminate\Queue\SerializesModels;

class contact extends Mailable
{
    use Queueable, SerializesModels;

    /**
     * Create a new message instance.
     *
     * @return void
     */
    public function __construct($unEmail, $unSujet, $unTexte)
    {
        $this->email = $unEmail;
        $this->sujet = $unSujet;
        $this->texte = $unTexte;
    }


    /**
     * Build the message.
     *
     * @return $this
     */
    public function build()
    {
        return $this->subject("VeloClub")->view('mails.contact', [
            "email" => $this->email,
            "sujet" => $this->sujet,
            "texte" => $this->texte
        ]);
    }

}
